#ifndef MESSAGE_H
#define MESSAGE_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct Vec2D
{
  float x;
  float y;
} Vec2D;

enum MsgType
{
  QUERY = 1,
  RESULT = 2,
  ACTION = 3
};

enum ActionType
{
  CREATE_ENTITY,
  DESTROY_ENTITY,
  APPLY_ENTITY,
  QUIT
};

enum DataType
{
  NOT_INIT,
  BOOL,
  INT,
  FLOAT,
  VEC2D,
  STRING,
  LIST,
  LISTICLE
};

enum Property
{
  POSITION,
  ANGLE,
  ANGULAR_VELOCITY,
  LINEAR_VELOCITY,

  SHAPE,
  DIMENSIONS,
  BODY_TYPE,
  FIXTURE_IDX
};

enum Shape
{
  CIRCLE,
  RECTANGLE,
  CHAIN
};

enum Color
{
    GREEN,
    BLUE
};

enum BodyType
{
  STATIC,
  DYNAMIC
};


enum Accessor
{
  NULL_PROP,
  ID,
  ACTION_TYPE,
  LIST_ELEMENT,
  
  GET_POSITION,
  GET_ANGLE,
  GET_FIXTURE,
  GET_TYPE,
  GET_RESTITUTION,
  GET_DENSITY,
  GET_FRICTION,

  SET_POSITION,
  SET_ANGLE,
  SET_RESTITUTION,
  SET_DENSTY,
  SET_FIXTURE,
  SET_FRICTION,
  SET_TYPE,
  SET_SHAPE,
  SET_COLOR
};

typedef struct Value
{
  enum DataType data_type;
  union
  {
    int v_int;
    float v_float;
    char v_str[80];
    Vec2D v_vec2d;
    int v_list_int[20];
    float v_list_float[20];
    Vec2D v_list_vec2d[20];
  };
} Value;

typedef struct MsgData
{
  long mtype;
  int op_type;
  int keys[10];
  Value vals[10];
} MsgData;

typedef struct Payload
{
  long mtype;
  int id;
  Value data[10];
} Payload;

typedef struct TestStruct
{
  long mtype;
  char mtext[80];
} TestStruct;

typedef struct Query
{
  long mtype;
  int id;
  int properties[10];
} Query;

typedef struct Action
{
  long mtype;
  int id;
  int action_type;
  int properties[10];
  Value values[10];
} Action;

typedef struct MsgQueue
{
  int id;
  key_t key;
} MsgQueue;

#ifdef __cplusplus
}
#endif

int get_msgqu_id ();

#endif // MESSAGE_H
