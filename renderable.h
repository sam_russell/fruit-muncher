#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "message.h"

namespace muncher
{
class Renderable
{
public:
    Renderable(int type, int id) : type{type}, id{id}
    { /* NOP */ }
    std::unordered_map<int, Value> user_data;
    int type;
    const int id;
    union
    {
        sf::Transformable* transformable;
        sf::VertexArray* vertex_array;
    };
};
}
#endif // RENDERABLE_H
