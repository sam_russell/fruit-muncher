#ifndef PHYSICSACCESSOR_H
#define PHYSICSACCESSOR_H

#include <vector>

#include "Box2D/Box2D.h"

#include "message.h"
#include "shapeprovider.h"

namespace muncher
{

  b2Body* get_body_by_id(b2World*, int);
  b2Fixture* get_fixture_by_idx(b2World*, int, int);


  class PhysicsAccessor
  {
  public:
    PhysicsAccessor () {}
    virtual ~PhysicsAccessor() {}
    /* setters */
    virtual void operator () (int, Value) {}
    virtual void operator () (b2BodyDef&, Value) {}
    virtual void operator () (b2Body*, Value) {}
    virtual void operator () (b2Body*, b2FixtureDef&) {}
    virtual void operator () (b2Fixture*, Value) {}
    virtual void operator () (b2FixtureDef&, Value) {}
    virtual void operator () (b2Fixture*, int, Value) {}
    virtual void operator () (b2FixtureDef&, int, Value) {}

    /* fixture setters */
    virtual void operator () (int, int, Value) {}

    /* getters */
    virtual Value operator () (b2Body*) { return Value(); }
    virtual Value operator () (b2Fixture*) { return Value(); }
  };

  class PositionSet: public PhysicsAccessor
  {
  public:
    PositionSet() {}
    virtual ~PositionSet () {}
    void operator () (b2BodyDef&, Value);
  };

  class PositionGet: public PhysicsAccessor
  {
  public:
    PositionGet() {}
    virtual ~PositionGet () {}
    Value /* VEC2D */ operator () (b2Body*);
  };

  class AngleGet: public PhysicsAccessor
  {
  public:
    AngleGet() {}
    virtual ~AngleGet () {}
    Value /* float */ operator () (b2Body*);
  };

  class AngleSet: public PhysicsAccessor
  {
  public:
    AngleSet() {}
    virtual ~AngleSet () {}
    void operator () (b2BodyDef&, Value);
  };

  class TypeSet: public PhysicsAccessor
  {
  public:
    TypeSet() {}
    virtual ~TypeSet () {}
    void operator () (b2Body*, Value);
    void operator () (b2BodyDef&, Value);
  };

  class TypeGet: public PhysicsAccessor
  {
  public:
    TypeGet() {}
    virtual ~TypeGet () {}
    Value operator () (b2Body*);
    Value operator () (b2BodyDef&);
  };

  class FixtureSet: public PhysicsAccessor
  {
  public:
    FixtureSet() {}
    virtual ~FixtureSet () {}
    void operator () (b2Body*, b2FixtureDef&);
  };

  class FixtureGet: public PhysicsAccessor
  {
  public:
    FixtureGet() {}
    virtual ~FixtureGet () {}
    /* returns ID of the fixture */
    Value operator () (int);
  };

  class RestitutionSet: public PhysicsAccessor
  {
  public:
    RestitutionSet() {}
    virtual ~RestitutionSet () {}
    void operator () (b2Fixture*, Value);
    void operator () (b2FixtureDef&, Value);
  };

  class RestitutionGet: public PhysicsAccessor
  {
  public:
    RestitutionGet() {}
    virtual ~RestitutionGet () {}
    Value operator () (b2Fixture*);
  };

  class DensitySet: public PhysicsAccessor
  {
  public:
    DensitySet () {}
    virtual ~DensitySet () {}
    void operator () (b2Fixture*, Value);
    void operator () (b2FixtureDef&, Value);
  };

  class DensityGet: public PhysicsAccessor
  {
  public:
    DensityGet() {}
    virtual ~DensityGet () {}
    Value operator () (b2Fixture*);
  };

  class FrictionSet: public PhysicsAccessor
  {
  public:
    FrictionSet () {}
    virtual ~FrictionSet () {}
    void operator () (b2Fixture*, Value);
    void operator () (b2FixtureDef&, Value);
  };

  class FrictionGet: public PhysicsAccessor
  {
  public:
    FrictionGet() {}
    virtual ~FrictionGet () {}
    Value operator () (b2Fixture*);
  };

  class ShapeSet: public PhysicsAccessor
  {
  public:
    ShapeSet() {}
    virtual ~ShapeSet () {}
    void operator () (b2Fixture*, int, Value);
    void operator () (b2FixtureDef&, int, Value);
  };


}
#endif // PHYSICSACCESSOR_H
