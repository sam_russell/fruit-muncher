#ifndef SHAPEPROVIDER_H
#define SHAPEPROVIDER_H

#include "Box2D/Box2D.h"

#include "message.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

namespace muncher
{
  class Shape
  {
  private:
    void* m_user_data;
  public:
    Shape() {}
    Shape(int t)
      : shape_type{t} { /* NOP */ }
    Shape(int t, void* data)
      : shape_type{t}, m_user_data{data}
    { /* NOP */ }
    int shape_type;
    void set_user_data(void*);
    void* get_user_data();
  };

  class ShapeProvider
  {
  public:
    virtual ~ShapeProvider() {}
    virtual Shape* make_circle(Value);
    virtual Shape* make_rectangle(Value);
    virtual Shape* make_chain(Value);
  };

}

#endif // SHAPEPROVIDER_H
