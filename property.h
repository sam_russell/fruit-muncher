#ifndef PROPERTY_H
#define PROPERTY_H

#include <iostream>
#include <list>
#include <map>

#include <SFML/Graphics.hpp>

namespace muncher
{
    enum Type
    {
        INT,
        FLOAT,
        VEC_2D,
        BODY,
        TRANSFORMABLE,
        RENDERABLE
    };

    enum EntityType
    {
        GROUND
    };

    enum PropertyType
    {
        ENTITY_TYPE,
        POSITION,
        ANGLE,
    };

    struct Property
    {
        enum PropertyType type;
        std::list<int> domain;
        std::list<int> range;
    };

    struct Value
    {
        union
        {
            int m_int;
            float m_float;
            sf::Vector2f* m_vec2d;
        };
    };

    struct TypedValue
    {
        enum Type type;
        Value value;
        TypedValue() {}
        TypedValue(float v) : type{FLOAT}
        {
            Value val;
            val.m_float = v;
            value = val;
        }
        TypedValue(int v) : type{INT}
        {
            Value val;
            val.m_int = v;
            value = val;
        }
        TypedValue(sf::Vector2f v) : type{VEC_2D}
        {
            Value val;
            val.m_vec2d = &v;
            value = val;
        }
        bool operator == (TypedValue* t)
        {
            return this->value == t->value;
        }

    };

    struct Renderable
    {
        std::map<int, TypedValue*> user_data;
    };
    struct Contact {};

    struct Body {};
    struct Transformable {};

    Property position { POSITION, {BODY, TRANSFORMABLE}, {VEC_2D} };
    Property entity_type {ENTITY_TYPE, {RENDERABLE}, {INT} };

    template <class T>
    T TypeLookup<T> (TypedValue* v)
    {
        int t = v->type;
        switch (t)
            {
                case INT:
                    return v->m_int;
            }
    }

    class PositionFunctor
    {
    public:
        PositionFunctor () {}
        sf::Vector2f operator () (Body);
        sf::Vector2f operator () (Transformable);
    };

    std::list<TypedValue> hasRelationValue(Renderable* s, int p, TypedValue* o)
    {
        TypedValue* tv = s->user_data[p];
    }


    std::list<TypedValue> v = hasRelationValue(r, ENTITY_TYPE, TypedValue(GROUND) );

}


#endif // PROPERTY_H
