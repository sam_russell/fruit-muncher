#ifndef PHYSICS_H
#define PHYSICS_H

#include <iostream>
#include <queue>
#include <unordered_map>
#include <time.h>

#include "Box2D/Box2D.h"

#include "message.h"
#include "event.h"
#include "physicsaccessor.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

namespace muncher
{
  static int BodyTypeLookup[2]
  { STATIC, DYNAMIC };



  class PhysicsLoop
  {
  private:
    b2World* m_world;
    MsgQueue m_mq;
    long m_inbox;
    long m_outbox;

    MsgData handle_msg(MsgData);
    MsgData handle_query(MsgData);
    MsgData handle_action(MsgData);

    Value create_entity(MsgData);
    Value create_entity(MsgData, MsgData);

    void modify_entity(MsgData);

    b2Body* make_body(int, int);
    b2Body* make_body(int, int, Vec2D);

    ShapeProvider m_shape_provider;

    std::unordered_map<int, PhysicsAccessor*> m_acc_tab;

    bool m_quit;
  public:
    PhysicsLoop(b2World* w, MsgQueue mq)
        : m_world{w}, m_mq{mq}, m_inbox{2}, m_outbox{1}
    {
      m_quit = false;
    }
    virtual void init_accessor_tab();
    //ShapeProvider* shape_provider;
    PhysicsAccessor* get_accessor(int);
    b2Body* get_body_by_id(int);

    bool poll_msg(MsgData&);
    void run();
  };

}

#endif
