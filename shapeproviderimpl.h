#ifndef SHAPEPROVIDERIMPL_H
#define SHAPEPROVIDERIMPL_H

#include "Box2D/Box2D.h"

#include "shapeprovider.h"

namespace muncher
{

class ShapeProviderImpl: public ShapeProvider
{
public:
  ShapeProviderImpl() {}
  virtual ~ShapeProviderImpl () {}
  virtual Shape* make_circle(Value);
  virtual Shape* make_rectangle(Value);
  virtual Shape* make_chain(Value);

  virtual b2CircleShape* get_circle(Value);
  virtual b2PolygonShape* get_rectangle(Value);
  virtual b2ChainShape* get_chain(Value);
};

}
#endif // SHAPEPROVIDERIMPL_H
