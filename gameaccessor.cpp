#include "gameaccessor.h"

namespace mu = muncher;

/* virtual */
void game::PositionSet::operator () (mu::Renderable* entity, Value v)
{
    assert(v.data_type == VEC2D);
    std::cout << "data type verified" << std::endl;
    entity->transformable->setPosition(
                v.v_vec2d.x,
                v.v_vec2d.y);
    std::cout << "position set" << std::endl;
}

/* instead of two values, need one LIST value instead...
 so,
TODO: implement LIST Value
*/
void game::ShapeSet::operator() (mu::Renderable* entity,
                               std::vector<Value> vals)
/*
 * first elmt is shape type
 * second elmt is shape size
 */
{
  std::cout << "trying to set shape" << std::endl;
  std::cout << "there are " << vals.size() << " args" << std::endl;
    assert(vals[0].data_type == INT);
    int t = vals[0].v_int;
    Value v = vals[1];
    std::cout << "value data type: " << v.data_type << std::endl;
    switch (t)
    {
    case CIRCLE:
        assert(v.data_type == FLOAT);
        entity->transformable = static_cast<sf::Transformable*>
                (new sf::CircleShape(v.v_float) );
        break;
    case RECTANGLE:
        assert(v.data_type == VEC2D);
        entity->transformable = static_cast<sf::Transformable*>
                (new sf::RectangleShape(
                     sf::Vector2f(v.v_vec2d.x,
                                  v.v_vec2d.y)) );
        break;
    case CHAIN:
        assert(v.data_type == LIST);
        std::cout << "not implemented yet!! :)" << std::endl;
    }
    std::cout << "shape has been set" << std::endl;
}

/* virtual */
void game::ColorSet::operator() (mu::Renderable* entity, Value v)
{
    assert(v.data_type == INT);
    int color = v.v_int;
    sf::Shape* s = dynamic_cast<sf::Shape*>
            (entity->transformable);
    switch (color)
    {
    case BLUE:
        s->setFillColor(sf::Color::Blue);
        break;
    case GREEN:
        s->setFillColor(sf::Color::Green);
        break;
    }
    entity->transformable = (sf::Transformable*) s;
}
