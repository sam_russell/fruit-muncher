#include "shapeproviderimpl.h"

namespace mu = muncher;

mu::Shape* mu::ShapeProviderImpl::make_circle(Value v)
{
  b2CircleShape* c = new b2CircleShape;
  int t = v.data_type;
  switch (t)
    {
    case Msg::FLOAT:
      c->m_radius = v.v_float;
      break;
    case Msg::VEC2D:
      /* TODO: figure out how to safely compare floats
      assert(v.v_vec2d.x == v.v_vec2d.y);
      */
      c->m_radius = v.v_vec2d.x;
      break;
    }
  return new Shape(Msg::CIRCLE, (void*) c);
}

mu::Shape* mu::ShapeProviderImpl::make_rectangle(Value v)
{
  b2PolygonShape* rect = new b2PolygonShape;
  float w = v.v_vec2d.x / 2;
  float h = v.v_vec2d.y / 2;
  rect->SetAsBox(w, h);
  return new Shape(Msg::RECTANGLE, (void*) rect);
}

mu::Shape* mu::ShapeProviderImpl::make_chain(Value v)
{
  int num_verts = ARRAY_SIZE(v.v_list_vec2d);
  b2Vec2 verts[num_verts];

  b2ChainShape* ch = new b2ChainShape;
  for (int i = 0; i < num_verts; ++i)
    {
      Vec2D generic = v.v_list_vec2d[i];
      b2Vec2 spec = b2Vec2(generic.x, generic.y);
      verts[i] = spec;
    }
  ch->CreateChain(verts, num_verts);
  return new Shape(Msg::CHAIN, (void*) ch);
}

b2CircleShape* mu::ShapeProviderImpl::get_circle(Value v)
{
  Shape* sh = make_circle(v);
  b2CircleShape* c = (b2CircleShape*) sh->get_user_data();
  return c;
}

b2PolygonShape* mu::ShapeProviderImpl::get_rectangle(Value v)
{
  Shape* sh = make_rectangle(v);
  b2PolygonShape* rect = (b2PolygonShape*) sh->get_user_data();
  return rect;
}

b2ChainShape* mu::ShapeProviderImpl::get_chain(Value v)
{
  Shape* sh = make_chain(v);
  b2ChainShape* ch = (b2ChainShape*) sh->get_user_data();
  return ch;
}

