#include "physicsaccessor.h"
#include "shapeprovider.h"

namespace mu = muncher;


b2Body* mu::get_body_by_id(b2World* w, int id)
{
  b2Body* b;
  for (b = w->GetBodyList(); b; b->GetNext() )
    {
      int* body_id = (int*) b->GetUserData();
      if (*body_id == id)
        {
          return b;
        }
    }
  return b;
}

b2Fixture* mu::get_fixture_by_idx(b2World* w, int id, int idx)
{
  b2Body* b = get_body_by_id(w, id);
  b2Fixture* f;
  int cur_idx = 0;
  for (f = b->GetFixtureList(); f; f->GetNext() )
    {
      if (cur_idx == idx)
        {
          return f;
        }
      else
        {
          ++idx;
        }
    }
  return f;
}

b2CircleShape* make_circle(mu::ShapeProvider fac, Value v)
{
  mu::Shape* sh = fac.make_circle(v);
  return (b2CircleShape*) sh;
}

b2PolygonShape* make_rectangle(mu::ShapeProvider fac, Value v)
{
  mu::Shape* sh = fac.make_circle(v);
  return (b2PolygonShape*) sh;
}

b2ChainShape* make_chain(mu::ShapeProvider fac, Value v)
{
  mu::Shape* sh = fac.make_circle(v);
  return (b2ChainShape*) sh;
}

/* virtual */
Value mu::TypeGet::operator () (b2Body* b)
{
  Value v;
  v.data_type = INT;
  int t = b->GetType();
  switch (t)
    {
    case b2_staticBody:
      v.v_int = STATIC;
      break;
    case b2_dynamicBody:
      v.v_int = DYNAMIC;
      break;
    }
  return v;
}

/* virtual */
void mu::TypeSet::operator () (b2BodyDef& b, Value v)
{
  int t = v.v_int;
  switch (t)
    {
      case DYNAMIC:
      b.type = b2_dynamicBody;
      break;
    case STATIC:
      b.type = b2_staticBody;
      break;
    }
}

/* virtual */
void mu::TypeSet::operator () (b2Body* b, Value v)
{
  int t = v.v_int;
  switch (t)
    {
      case DYNAMIC:
      b->SetType(b2_dynamicBody);
      break;
    case STATIC:
      b->SetType(b2_staticBody);
      break;
    }
}

/* virtual */
void mu::PositionSet::operator () (b2BodyDef& b, Value v)
{
  Vec2D pos = v.v_vec2d;
  b.position = b2Vec2(pos.x, pos.y);
}


/* virtual */
Value mu::PositionGet::operator () (b2Body* b)
{
  b2Vec2 pos = b->GetPosition();
  return Value {.data_type=VEC2D,
          .v_vec2d={pos.x, pos.y} };
}

/* virtual */
void mu::AngleSet::operator () (b2BodyDef& b, Value v)
{
  b.angle = v.v_float;
}

/* virtual */
Value mu::AngleGet::operator () (b2Body* b)
{
  return Value { .data_type=FLOAT, .v_float = b->GetAngle() };
}


/* virtual */
void mu::ShapeSet::operator()(b2FixtureDef& fdef,
                              int t,
                              Value v)
{
  ShapeProvider shapes;
  switch (t)
    {
    case CIRCLE:
      fdef.shape = make_circle(shapes, v);
      break;
    case RECTANGLE:
      fdef.shape = make_rectangle(shapes, v);
      break;
    case CHAIN:
      fdef.shape = make_chain(shapes, v);
      break;
    }
}

/* virtual */
void mu::FixtureSet::operator () (b2Body* b, b2FixtureDef& f)
{
  b->CreateFixture(&f);
}

/* virtual */
Value mu::RestitutionGet::operator()(b2Fixture * f)
{
  return Value {.data_type = FLOAT,
    .v_float = f->GetRestitution() };
}

/* virutal */
void mu::RestitutionSet::operator()(b2Fixture* f, Value v)
{
  f->SetRestitution(v.v_float);
}

/* virutal */
void mu::RestitutionSet::operator()(b2FixtureDef& f, Value v)
{
  f.restitution = v.v_float;
}

/* virtual */
Value mu::DensityGet::operator()(b2Fixture* f)
{
  return Value { .data_type=FLOAT,
          .v_float = f->GetDensity() };
}

void mu::DensitySet::operator()(b2Fixture * f, Value v)
{
  f->SetDensity(v.v_float);
}

void mu::DensitySet::operator()(b2FixtureDef& f, Value v)
{
  f.density = v.v_float;
}

Value mu::FrictionGet::operator () (b2Fixture* f)
{
  return Value { .data_type = FLOAT,
          .v_float = f->GetFriction() };
}

void mu::FrictionSet::operator()(b2Fixture * f, Value v)
{
  f->SetFriction(v.v_float);
}

void mu::FrictionSet::operator()(b2FixtureDef & f, Value v)
{
  f.friction = v.v_float;
}
