#ifndef GAMEACCESSOR_H
#define GAMEACCESSOR_H

#include <iostream>
#include <assert.h>
#include <vector>

#include <SFML/Graphics.hpp>

#include "message.h"
#include "renderable.h"

namespace mu = muncher;

namespace game
{
class GameAccessor
{
public:
    GameAccessor() {}
    virtual ~GameAccessor() {}

    /* setters */
    virtual void operator () (int, Value) {}
    virtual void operator () (mu::Renderable*, Value) {}
    virtual void operator () (mu::Renderable*,
                              std::vector<Value>) {}

    /* getters */
    virtual Value operator () (Value) { return Value(); }
};

class PositionSet: public GameAccessor
{
  public:
    PositionSet() {}
    virtual ~PositionSet () {}
    void operator () (mu::Renderable*, Value);
};

class ShapeSet: public GameAccessor
{
public:
    ShapeSet() {}
    virtual ~ShapeSet () {}
    void operator () (mu::Renderable*, std::vector<Value>);
};

class ColorSet: public GameAccessor
{
public:
    ColorSet() {}
    virtual ~ColorSet () {}
    void operator () (mu::Renderable*, Value);
};

}
#endif // GAMEACCESSOR_H
