#include "physics.h"

namespace mu = muncher;

bool mu::PhysicsLoop::poll_msg(MsgData& md)
{
  MsgData* buf = new MsgData;
  ssize_t rec = msgrcv(m_mq.id,
                       buf,
                       sizeof(MsgData),
                       m_inbox,
                       IPC_NOWAIT);
  memcpy(&md, buf, sizeof(MsgData) );
  if (rec == -1)
    {
      free (buf);
      return false;
    }
  else
    {

      free (buf);
      return true;
    }
}

b2Body* mu::PhysicsLoop::get_body_by_id(int id)
{
  b2Body* b;
  for (b = m_world->GetBodyList(); b; b = b->GetNext() )
    {
      int* body_id = (int*) b->GetUserData();
      if (*body_id == id)
        {
          return b;
        }
    }
}

/* can be be overidden to supply your own accessors */
/*virtual*/ void mu::PhysicsLoop::init_accessor_tab()
{
  /* position */
  m_acc_tab[GET_POSITION] = new mu::PositionGet();
  m_acc_tab[SET_POSITION] = new mu::PositionSet();

  /* angle */
  m_acc_tab[GET_ANGLE] = new mu::AngleGet();
  m_acc_tab[SET_ANGLE] = new mu::AngleSet();

  /* type */
   m_acc_tab[SET_TYPE] = new mu::TypeSet();
   m_acc_tab[GET_TYPE] = new mu::TypeGet();

   /* fixture */
   m_acc_tab[GET_FIXTURE] = new mu::FixtureGet();
   m_acc_tab[SET_FIXTURE] = new mu::FixtureSet();

   /* restitution */
   m_acc_tab[GET_RESTITUTION] = new mu::RestitutionGet();
   m_acc_tab[SET_RESTITUTION] = new mu::RestitutionSet();

   /* density */
   m_acc_tab[GET_DENSITY] = new mu::DensityGet();
   m_acc_tab[SET_DENSTY] = new mu::DensitySet();

   /* friction */
   m_acc_tab[GET_FRICTION] = new mu::FrictionGet();
   m_acc_tab[SET_FRICTION] = new mu::FrictionSet();
}


MsgData mu::PhysicsLoop::handle_msg(MsgData md)
{
  MsgData result;
  int t;
  t = md.op_type;
  switch (t)
    {
    case QUERY:
      result = handle_query(md);
      break;
    case ACTION:
      result = handle_action(md);
      break;
    }
  return result;
}

mu::PhysicsAccessor* mu::PhysicsLoop::get_accessor(int tag)
{
  mu::PhysicsAccessor* acc = m_acc_tab[tag];
  return acc;
}


MsgData mu::PhysicsLoop::handle_query(MsgData md)
{
    std::cout << "handling query" << std::endl;
  MsgData result;
  /* first property must be entity id */
  int entity_id;
  entity_id = md.vals[0].v_int;
  std::cout << "the id is: " << entity_id << std::endl;
  std::cout << "the accessor key is: " << md.keys[1]
            << std::endl;
  b2Body* b = get_body_by_id(entity_id);
  std::cout << "retrieved body for entity "
            << entity_id << std::endl;
  for (int i = 1; i < ARRAY_SIZE(md.keys); ++i)
    {
      if (md.keys[i] == NULL_PROP)
        {
          break;
        }
      int prop = md.keys[i];
      mu::PhysicsAccessor* acc = get_accessor(prop);
      result.vals[i] = (*acc)(b);
      //memcpy(&result.vals[i], &v, sizeof(Value) );
      result.keys[i] = md.keys[i];
    }
  result.mtype = entity_id * 10;
  result.op_type = RESULT;
  int sent = msgsnd (m_mq.id, &result, sizeof (MsgData), 0);
  std::cout << "vee int: " << result.vals[1].v_int << std::endl;
  std::cout << "sent: " << sent << std::endl;
  return result;
}





//b2Body* mu::PhysicsLoop::make_body(int id, int t)
//{
//  b2BodyDef bdef;
//  //bdef.type
//}

//b2Body* mu::PhysicsLoop::make_body(int id, int t, Vec2D)
//{

//}

Value mu::PhysicsLoop::create_entity(MsgData init_data)
{
  int begin_data = 2;
  int id_col = 1;
  b2BodyDef bdef;
  // should there be an ID accessor for setting the ID?
  // ...probably should, i think :)
  for (int i = begin_data; i < ARRAY_SIZE(init_data.keys); ++i)
    {
      int label = init_data.keys[i];
      if (label > 0)
        {
          PhysicsAccessor* acc = get_accessor(label);
          std::cout << "using accessor " << label << std::endl;
          acc->operator()(bdef, init_data.vals[i]);
        }
    }
  int id = init_data.vals[id_col].v_int;
  b2Body* b = m_world->CreateBody(&bdef);
  b->SetUserData( (void*) &id );
  return init_data.vals[id_col];
}


MsgData mu::PhysicsLoop::handle_action(MsgData md)
{
  for (int i = 0; i < ARRAY_SIZE(md.vals); ++i)
    {
      std::cout << md.vals[i].data_type << std::endl;
    }
  std::cout << "handling action" << std::endl;
  MsgData result;
  int action_t = md.vals[0].v_int;
  switch (action_t)
    {
    case CREATE_ENTITY:
      result.keys[0] = ID;
      result.vals[0] = create_entity(md);
      break;
    case QUIT:
      m_quit = true;
      break;
    }
  return result;
}


void mu::PhysicsLoop::run()
{
  struct timespec t {0, 1000000L};
  bool tock = false;
  while (!m_quit)
    {
      nanosleep(&t, NULL);

      MsgData msg_in;
      while ( poll_msg(msg_in) )
        {
          for (int i = 0; i < ARRAY_SIZE(msg_in.vals); ++i)
            {
              std::cout << msg_in.vals[i].data_type << std::endl;
              if (msg_in.vals[i].data_type == 2)
                {
                  std::cout << "duh value is: " << msg_in.vals[i].v_int << std::endl;
                }
            }
          handle_msg(msg_in);
        }

      /* do timestep here */
      m_world->Step(1.0f/20.0f, 10, 8);

      /*
      if (tock == false)
      {
        std::cout << "TICK!" << std::endl;
        tock = true;
      }
      else
      {
       std::cout << "TOCK!" << std::endl;
       b2Body* dudeman = get_body_by_id(1);
       if (dudeman)
         {
           b2Vec2 pos = dudeman->GetPosition();
           std::cout << " a dudeman at " << pos. x << ", " << pos.y << std::endl;
         }
       //b2Vec2 pos = dudeman->GetPosition();
       //std::cout << pos.x << ", " << pos.y << std::endl;
       tock = false;
      }
      */

    }
  std::cout << "ok see you bud" << std::endl;
}

