#include "event.h"

namespace mu = muncher;

void mu::Event::set_init_status(bool status)
{
    m_initialized = status;
}

mu::Event::operator bool()
{
    if (this->m_initialized == true)
    {
        return true;
    }
    else
    {
        return false;
    }
}
