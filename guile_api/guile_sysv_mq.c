/*
  __________________
 / (c) Sam Russell  \
| GPLv3              |
 \ pizzapal@sdf.org /
  ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <libguile.h>

#include "../message.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]) )
#endif


int start_queue (key_t k)
{
  return msgget (k, IPC_CREAT | 0660);
}


static SCM s_start_queue (SCM ftok_key)
{
  key_t key;
  char* fpath = scm_to_locale_string (ftok_key);
  key = ftok (fpath, 3);
  int id;
  id = start_queue (key);
  SCM result = scm_from_int (id);
  return result;
}


Value* make_value (Value* v, SCM type, SCM val)
{
  v = malloc (sizeof (Value) );
  int t;
  t = scm_to_int (type);
  v->data_type = t;
  switch (t)
    {
    case BOOL:
      v->v_int = scm_to_bool (val);
      break;
    case INT:
      v->v_int = scm_to_int (val);
      break;
    case FLOAT:
      v->v_float = scm_to_double (val);
      break;
    case VEC2D:
      /* i think this may be skipping a level of access */
      v->v_vec2d.x = scm_to_double (scm_car (val) );
      v->v_vec2d.y = scm_to_double (scm_cdr (val) );
      break;
    case STRING:
      strcpy (v->v_str, scm_to_locale_string (val) );
      break;
    case LISTICLE:
      v->v_int = scm_to_int (scm_length (val) );
      //printf ("listicle length: %d\n", v->v_int);
      break;
    }
  return v;
}

SCM from_value (Value v)
{
  int val_t;
  val_t = v.data_type;
  SCM s_item;
  switch (val_t)
    {
    case VEC2D:
      s_item = scm_cons (scm_from_double (v.v_vec2d.x),
			 scm_from_double (v.v_vec2d.y) );
      break;
    case FLOAT:
      s_item = scm_from_double (v.v_float);
      break;
    case INT:
      s_item = scm_from_int (v.v_int);
      break;
    }
  return s_item;
}

SCM s_get_query_result (SCM mq_id, SCM m_type)
{
  long msg_t;
  msg_t = scm_to_long (m_type);

  MsgData q;
  
  ssize_t q_size;
  q_size = msgrcv ( scm_to_int (mq_id),
		    &q,
		    sizeof (MsgData),
		    msg_t,
		    0);

  if (q_size == -1)
    {
      return scm_from_utf8_string ( strerror (errno) );
    }
  else
    {
      SCM lst = SCM_EOL;
      int num_cols;
      //printf ("got a response!!!\n");
      for (int i = 0; i < ARRAY_SIZE (q.keys); ++i)
	{
	  //printf ("%d\n", q.keys[i]);
	  if (q.keys[i] == 0)
	    {
	      num_cols = i;
	      break;
	    }
	}
      //printf ("num cols: %d\n", num_cols);
      
      for (int i = 0; i < num_cols; ++i)
	{
	  lst = scm_cons ( from_value (q.vals[i]) , lst);
	}
      return scm_reverse (lst);
    }
}


SCM s_make_query (SCM mq_id, SCM m_type, SCM id, SCM blocking, SCM props)
{
  int props_count;
  props_count = scm_to_int ( scm_length (props) );
  int props_array[props_count + 1];
  //printf ("%d\n", props_count);

  long msg_t;
  msg_t = scm_to_long (m_type);

  int entity_id;
  entity_id = scm_to_int (id);
  Value v;
  v.data_type = INT;
  v.v_int = entity_id;

  MsgData q;
  q.mtype = msg_t;
  q.op_type = QUERY;
  q.keys[0] = ID;
  q.vals[0] = v;

  for (int i = 0; i < props_count; ++i)
    {
      SCM s_item;
      s_item = scm_list_ref (props, scm_from_int (i) );
      q.keys[i + 1] = scm_to_int (s_item);
      //printf ("%d\n", q.keys[i]);
    }
  
  int sent;
  sent = msgsnd ( scm_to_int (mq_id), &q, sizeof (MsgData), 0);

  int blk;
  blk = scm_to_int (blocking);
  if (blk == 0)
    {
      return scm_from_int (sent);
    }
  else
    {
      SCM mbox_id;
      int mbox;
      mbox = entity_id * 10;
      mbox_id = scm_from_int (mbox);
      return s_get_query_result (mq_id, mbox_id);
    }
}



SCM s_make_action (SCM mq_id, SCM m_type, SCM values)
{
  int args_count;
  args_count = scm_to_int ( scm_length (values) );
  //printf ("length of values list: %d\n", args_count);

  long msg_t;
  msg_t = scm_to_long (m_type);

  MsgData m;
  m.mtype = msg_t;
  m.op_type = ACTION;

  /* LISTICLE is making the for loop terminate early because
the listicle elements are not included in the `args_count` since they
represent a nested list!!!

TODO: fix Listicle...
...can i change args_count in-place?  that sounds kinda bad, but if it works...
  */

  int current_val_idx;
  current_val_idx = 0;
  int current_key_idx;
  current_key_idx = 0;
  
  for (int i = 0; i < args_count; ++i)
    {
      SCM s_arg;
      /* s_arg must be a pair! */
      s_arg = scm_list_ref (values, scm_from_int (i) );
      int arg_tag;
      arg_tag = scm_to_int ( scm_car (s_arg) );

      SCM val_data;
      val_data = scm_cdr (s_arg);

      Value* v_buf;
      v_buf = make_value (v_buf, scm_car (val_data), scm_cdr (val_data) );
      /*
      printf ("arg-tag: %d\n", arg_tag);
      printf ("data-type: %d\n", v_buf->data_type);
      */
      memcpy ( (void*) &m.vals[current_val_idx],
	       (void*) v_buf,
	       sizeof (Value) );
      ++current_val_idx;
      if (v_buf->data_type == LISTICLE)
	{
	  m.keys[current_key_idx] = arg_tag;
	  ++current_key_idx;

	  int len;
	  len = v_buf->v_int;
	  int stop;
	  stop = i + len;
	  
	  for (int j = 1; j < stop; ++j)
	    {
	      /*
	      printf ("initializing list element %d of %d\n",
		      j,
		      v_buf->v_int);
	      */
	      Value* v_elmt_buf;
	      SCM s_elmt_pair;
	      s_elmt_pair = scm_list_ref (val_data, scm_from_int (j) );
	      v_elmt_buf = make_value (v_elmt_buf,
				   scm_car (s_elmt_pair),
				   scm_cdr (s_elmt_pair) );
	      
	      m.keys[current_key_idx] = LIST_ELEMENT;
	      ++current_key_idx;
	      //int idx;
	      //idx = i + j;
	      memcpy ( (void*) &m.vals[current_val_idx],
		       (void*) v_elmt_buf,
		       sizeof (Value) );
	      ++current_val_idx;
	    }
	}
      else
	{
	  m.keys[current_key_idx] = arg_tag;
	  ++current_key_idx;
	}
    }

  int sent;
  int id;
  id = scm_to_int (mq_id);
  sent = msgsnd (id, &m, sizeof (MsgData), 0);
  
  return scm_from_int (sent);
}


static void* register_functions (void* data)
{
  scm_c_define_gsubr ("start-queue", 1, 0, 0, &s_start_queue);
  scm_c_define_gsubr ("muncher-result", 2, 0, 0, &s_get_query_result);
  scm_c_define_gsubr ("muncher-query", 4, 0, 0, &s_make_query);
  scm_c_define_gsubr ("muncher-action", 3, 0, 0, &s_make_action);
  return NULL;
}

// "/home/sam/justanmq/some_file"

void init_sysv_mq ()
{
  scm_c_define_gsubr ("start-queue", 1, 0, 0, &s_start_queue);
  scm_c_define_gsubr ("muncher-result", 2, 0, 0, &s_get_query_result);
  scm_c_define_gsubr ("muncher-query", 5, 0, 0, &s_make_query);
  scm_c_define_gsubr ("muncher-action", 3, 0, 0, &s_make_action);
}


int main (int argc, char* argv[])
{
  scm_with_guile (&register_functions, NULL);
  scm_shell (argc, argv);
}
