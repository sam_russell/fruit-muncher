;;; (c) Sam Russell
;;; GPLv3
;;; pizzapal@sdf.org

(use-modules (srfi srfi-1))

(define mq-lib (dynamic-link "./guile-sysv-mq.so"))

(dynamic-call "init_sysv_mq" mq-lib)

(define (enumerate key-list)
  (fold acons '() key-list (iota (length key-list))))

(define (make-lookup lst)
  (lambda (x) (assoc-ref (enumerate lst) x)))



(define *accessors*
  '(#:null-prop
    #:id
    #:action-type
    #:list-element
    
    #:get-position
    #:get-angle
    #:get-fixture
    #:get-type
    #:get-restitution
    #:get-density
    #:get-friction
    
    #:set-position
    #:set-angle
    #:set-restitution
    #:set-density
    #:set-fixture
    #:set-friction
    #:set-type
    #:set-shape))

(define (accessors k) ((make-lookup *accessors*) k))


(define *action-type*
  '(#:create-entity
    #:destroy-entity
    #:apply-entity
    #:quit))

(define (action-type k) ((make-lookup *action-type*) k))


(define *message-type*
  '(#:query
    #:result
    #:action))

(define (message-type k) ((make-lookup *message-type*) k))


(define *data-type*
  '(#:not-init
    #:bool
    #:int
    #:float
    #:vec2d
    #:string
    #:list
    #:listicle))



(define (data-type k) ((make-lookup *data-type*) k))
(define (bool x) (cons (data-type #:bool) x))
(define (int x) (cons (data-type #:int) x))
(define (float x) (cons (data-type #:float) x))
(define (vec2d x y) (cons (data-type #:vec2d) (cons x y)))
(define (muncher-list t x) (cons (data-type #:list) (cons (data-type t) x)))
;; (define* (listicle #:rest lst) (cons (data-type #:listicle)
;;				     (prepare-action lst)))

(define *circle* (int 0))
(define *rectangle* (int 1))
(define *chain* (int 2))


(define (prepare-action r)
  (let ((vals (remove keyword? r))
	(props (remove (lambda (x) (not (keyword? x))) r)))
    (let ((props-enum (map (lambda (kw) (accessors kw)) props)))
      (map cons props-enum vals))))

(define* (listicle #:rest lst) (cons (data-type #:listicle)
				    lst))

(define (kv accr typed-val)
  (cons (accessors accr) typed-val))

  


;; EXAMPLE:
;;
;; (action #:create-entity
;; 	(set-type dynamic)
;; 	(set-shape circle 5.0)
;; 	(set-position 1.0. 1.0)
;; 	(set-restitution 0.85))
;;
;;  looks pretty clean and concise to me :)






(define* (action q-id m-type #:rest r)
;;  (prepare-action r))
 (muncher-action q-id m-type (prepare-action r)))


(define* (query q-id mbox-id entity-id block? #:rest properties)
  (let ((enum-props (map accessors properties)))
    (muncher-query q-id mbox-id entity-id block? enum-props)))


(define (circle radius x y)
  (action 32769 1
	  #:action-type (int 0)			    
	  #:set-shape (listicle *circle* (float radius))
	  #:set-position (vec2d x y)))

(define (rectangle w h x y)
  (action 32769 1
	  #:action-type (int 0)			    
	  #:set-shape (listicle *rectangle* (vec2d w h))
	  #:set-position (vec2d x y)))


(define (make-rect blk-size width height)
  (let lp ( (x 100)
	    (y 100) )
    (let ((sq (lambda (x y)
		(sleep 1)
		(rectangle blk-size
			   blk-size
			   x	y) )))
      (sq x y)
      (cond
       ( (and (= x 100)
	      (< y (+ 100 height)))
	 ;; ->
	 (lp x (+ y blk-size) ))
       ( (and (>= x 100)
	      (< x (+ width 100))
	      (>= y (+ 100 height)) )
	 ;; ->
	 (lp (+ x blk-size) y) )
       ( (and (>= x (+ width 100)) (> y 100)) 
	 ;; ->
	 (lp x (- y blk-size)) )
       ( (>= x (- 100 height))
	 ;; ->
	 (lp (- x blk-size) y) )
       ( (and (= 100 x) (= 100 y)) 'nil)))))



;;
;;(action 32769		2
;;	#:action-type	(int 0)
;;	#:id		(int 1)
;;	#:set-type	(int 1)
;;	#:set-position	(vec2d 1 . 0 1.5))
