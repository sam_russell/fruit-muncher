#ifndef EVENT_H
#define EVENT_H

#include "message.h"

namespace muncher {

class Event
{
    bool m_initialized;
public:
    Event(Action& a) : type{ACTION}, action{a} {}
    Event(Query& q) : type{QUERY}, query{q} {}
    enum MsgType type;
    union
    {
        Action action;
        Query query;
    };
    operator bool ();
    void set_init_status(bool);
};

}

#endif // EVENT_H
