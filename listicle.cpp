#include "listicle.h"
#include <iostream>

namespace mu = muncher;

std::vector<Value> mu::make_listicle(int start,
                                     int stop,
                                     Value vals[])
{
    std::vector<Value> result;
    for (int i = start; i < stop; ++i)
    {
        std::cout << "pushing value of type: "
                  << vals[i].data_type
                  << std::endl;
        result.push_back(vals[i]);
    }
    return result;
}
