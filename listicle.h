#ifndef LISTICLE_H
#define LISTICLE_H

#include <vector>

#include "message.h"


namespace muncher
{

std::vector<Value> make_listicle(int, int, Value[]);

}

#endif // LISTICLE_H
