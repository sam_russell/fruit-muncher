#ifndef GAMELOOP_H
#define GAMELOOP_H

#include <string.h>

#include <iostream>
#include <cstdlib>
#include <unordered_map>

#include <SFML/Graphics.hpp>

#include "listicle.h"
#include "message.h"
#include "renderable.h"
#include "gameaccessor.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

namespace muncher
{

class GameLoop
{
private:
    sf::RenderWindow* m_window;
    sf::Transform m_transform;

    MsgQueue m_mq;
    long m_inbox;
    long m_physics;
    long m_api;

    MsgData handle_msg(MsgData);
    void handle_update(MsgData);
    void handle_action(MsgData);
    MsgData handle_query(MsgData);

    Renderable* create_entity(MsgData);

    int send_msg(MsgData);

    std::unordered_map<int, game::GameAccessor*> m_game_acc;

    int m_renderable_counter;
    std::unordered_map<int, Renderable*> m_renderables;

    bool m_quit;

public:
    GameLoop(sf::RenderWindow* w, MsgQueue mq)
        : m_window{w}, m_mq{mq}, m_inbox{1}, m_physics{2},
          m_api{3}, m_renderable_counter{0}
    {
        m_quit = false;
    }
    virtual ~GameLoop () {}

    virtual void init_accessor_tab();

    game::GameAccessor* get_accessor(int);
    Renderable* get_renderable_by_id(int);

    bool poll_msg(MsgData&);
    void run();
};
}
#endif // GAMELOOP_H
