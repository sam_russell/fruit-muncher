#include "gameloop.h"

namespace mu = muncher;

void mu::GameLoop::init_accessor_tab()
{
    m_game_acc[SET_POSITION] = new game::PositionSet();
    m_game_acc[SET_SHAPE] = new game::ShapeSet();
    m_game_acc[SET_COLOR] = new game::ColorSet();
}

bool mu::GameLoop::poll_msg(MsgData& md)
{
  MsgData* buf = new MsgData;
  ssize_t rec = msgrcv(m_mq.id,
                       buf,
                       sizeof(MsgData),
                       m_inbox,
                       IPC_NOWAIT);
  memcpy(&md, buf, sizeof(MsgData) );
  if (rec == -1)
    {
      free (buf);
      return false;
    }
  else
    {
      std::cout << "received msg" << std::endl;

      free (buf);
      return true;
    }
}

MsgData mu::GameLoop::handle_msg(MsgData md)
{
    MsgData result;
    int t;
    t = md.op_type;
    switch (t)
      {
      case ACTION:
        handle_action(md);
        break;
      case QUERY:
        //result = handle_query(md);
        break;
      case RESULT:
        handle_update(md);
        break;
      }
    return result;
}

mu::Renderable* mu::GameLoop::create_entity(MsgData md)
{
    int id = ++m_renderable_counter;
    std::cout << "creating " << id << std::endl;
    Renderable* r = new Renderable(1, id);
    r->transformable = new sf::Transformable;
    //sf::CircleShape* c = new sf::CircleShape(10.0f);
    //c->setFillColor(sf::Color::Blue);
    //r->transformable = static_cast<sf::Transformable*>(c);

    int start_data = 1;
    for (int i = 0; i < ARRAY_SIZE(md.keys); ++i)
      {
        std::cout << "key " << i << ": " << md.keys[i] << std::endl;
      }
    for (int i = 0; i < ARRAY_SIZE(md.vals); ++i)
      {
        std::cout << "value data type: " << md.vals[i].data_type << std::endl;
      }

    for (int i = start_data; i < ARRAY_SIZE(md.vals); ++i)
    {
        std::cout << md.keys[i] << std::endl;
        if (md.keys[i] == 0)
        {
            break;
        }
        else
        {
            std::cout << "setting item at idx " << i << std::endl;
            std::cout << "bool val of item key: " << bool(md.keys[i]) << std::endl;
            /*
            if (md.vals[i].data_type == VEC2D)
              {
                std::cout << md.vals[i].v_vec2d.x
                          << ", "
                          << md.vals[i].v_vec2d.y
                          << std::endl;
              }
              */

        game::GameAccessor* acc = static_cast<game::GameAccessor*>(
                    get_accessor(md.keys[i]) );
        std::cout << acc << std::endl;
        std::cout << "data type: " << md.vals[i].data_type << std::endl;
        if (md.vals[i].data_type == LISTICLE)
        {
            std::cout << "its a dang listicle" << std::endl;
            int len = md.vals[i].v_int;
            std::cout << "listicle length is: " << len << std::endl;
            int start;
            int stop;
            start = i + 1;
            stop = start + len;
            std::vector<Value> listicle = mu::make_listicle(
                        start,
                        stop,
                        md.vals);
            (*acc)(r, listicle);
            std::cout << "stopping at " << stop << std::endl;
            i = stop - 1;
            std::cout << "i: " << i << std::endl;
        }
        else
          {
            (*acc)(r, md.vals[i]);
          }
        }
    }
    m_renderables[id] = r;
    return r;
}

void mu::GameLoop::handle_action(MsgData md)
{
  std::cout << "handling action" << std::endl;
    int t = md.vals[0].v_int;
    std::cout << t << std::endl;
    switch (t)
    {
    case CREATE_ENTITY:
        create_entity(md);
        break;
    }
}

game::GameAccessor* mu::GameLoop::get_accessor(int k)
{
    return m_game_acc[k];
}

void mu::GameLoop::handle_update(MsgData md)
{
    int update_type;
    update_type = md.vals[0].v_int;
    int entity_id;
    entity_id = md.vals[1].v_int;
    int begin_data = 2;
    Renderable* entity = m_renderables[entity_id];

    for (int i = begin_data; i < ARRAY_SIZE(md.vals); ++i )
    {
        game::GameAccessor* acc = get_accessor(md.keys[i]);

        if (md.vals[i].data_type == LISTICLE)
        {
            int start = md.vals[i].v_list_int[0];
            int stop = md.vals[i].v_list_int[1];
            std::vector<Value> listicle = mu::make_listicle(
                        start,
                        stop,
                        md.vals);
            (*acc)(entity, listicle);
            i = stop;
            std::cout << "stopping at " << stop << std::endl;
        }
        else
        {
            (*acc)(entity, md.vals[i]);
        }
    }
}

void mu::GameLoop::run()
{
    Renderable* r = new Renderable(1, 1);
    sf::RectangleShape* sh = new sf::RectangleShape(sf::Vector2f(5.0f, 5.0f));
    sh->setFillColor(sf::Color::Green);
    r->transformable = (sf::Transformable*) sh;
    r->transformable->setPosition(400.0f, 300.0f);
    m_renderables[0] = r;
    std::cout << "made a shape" << std::endl;
    // run the program as long as the window is open
       while (m_window->isOpen())
       {
           // check all the window's events that were triggered since the last iteration of the loop
           sf::Event event;
           while (m_window->pollEvent(event))
           {
               // "close requested" event: we close the window
               if (event.type == sf::Event::Closed)
                   m_window->close();
           }

           MsgData msg_in;
           while ( poll_msg(msg_in) )
           {
               handle_msg(msg_in);
           }

           // clear the window with black color
           m_window->clear(sf::Color::Black);

           for (int i = 0; i < m_renderables.size(); ++i)
           {
               //std::cout << "drawing " << i << std::endl;
               sf::Transformable* t = m_renderables[i]->transformable;
               sf::Drawable* d = dynamic_cast<sf::Drawable*>(t);
               m_window->draw(*d);
           }
           // draw everything here...
           // m_window->draw(...);

           // end the current frame
           m_window->display();
       }
}
