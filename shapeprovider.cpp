#include "shapeprovider.h"


namespace mu = muncher;

void* mu::Shape::get_user_data()
{
  return m_user_data;
}

void mu::Shape::set_user_data(void* data)
{
  m_user_data = data;
}

mu::Shape* mu::ShapeProvider::make_circle(Value v)
{
  b2CircleShape* c = new b2CircleShape;
  int t = v.data_type;
  switch (t)
    {
    case FLOAT:
      c->m_radius = v.v_float;
      break;
    case VEC2D:
      /* TODO: figure out how to safely compare floats
      assert(v.v_vec2d.x == v.v_vec2d.y);
      */
      c->m_radius = v.v_vec2d.x;
      break;
    }
  return new Shape(CIRCLE, (void*) c);
}

mu::Shape* mu::ShapeProvider::make_rectangle(Value v)
{
  b2PolygonShape* rect = new b2PolygonShape;
  float w = v.v_vec2d.x / 2;
  float h = v.v_vec2d.y / 2;
  rect->SetAsBox(w, h);
  return new Shape(RECTANGLE, (void*) rect);
}

mu::Shape* mu::ShapeProvider::make_chain(Value v)
{
  int num_verts = ARRAY_SIZE(v.v_list_vec2d);
  b2Vec2 verts[num_verts];

  b2ChainShape* ch = new b2ChainShape;
  for (int i = 0; i < num_verts; ++i)
    {
      Vec2D generic = v.v_list_vec2d[i];
      b2Vec2 spec = b2Vec2(generic.x, generic.y);
      verts[i] = spec;
    }
  ch->CreateChain(verts, num_verts);
  return new Shape(CHAIN, (void*) ch);
}
