#include <iostream>

#include <cstdlib>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <time.h>

#include "physics.h"
#include "gameloop.h"

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif

namespace mu = muncher;

int main(int argc, char* argv[])
{

  key_t key;
  key = ftok("muncher_ftok_file", 1);
  //key_t key = ftok( getenv("MUNCHER_KEY") , 3);
  //std::cout << key << std::endl;
  //std::cout << getenv("MUNCHER_KEY") << std::endl;
  int msgq_id = msgget(key, IPC_CREAT | 0770);
  std::cout << "msgget " << msgget(key, 0) << std::endl;
  std::cout << get_msgqu_id() << std::endl;

  std::cout << msgq_id << std::endl;

  MsgQueue msgq {get_msgqu_id(), key};

  b2World* w = new b2World(b2Vec2(0.0f, -10.0f) );

  mu::PhysicsLoop p = mu::PhysicsLoop(w, msgq);
  p.init_accessor_tab();
  //rcv_msg(msgq_id);
  /*
  rcv_msg();
  //p.run();


  struct timespec t {0, 1000000L};
  int msg_status = 0;
  while (msg_status < 1000000)
    {
      nanosleep (&t, NULL);
      msg_status += rcv_msg ();
    }
*/
  //p.run();
  sf::RenderWindow* win = new sf::RenderWindow(
              sf::VideoMode(800, 600),
              "muncher");
  mu::GameLoop g = mu::GameLoop(win, msgq);
  g.init_accessor_tab();
  g.run();

  return 0;
}
