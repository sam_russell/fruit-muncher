#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <vector>

namespace muncher{
class Constraint
{
public:
    Constraint() {}
    std::vector<int> funcs;
    void solve();
};

int relation_exists(int, /* predicate type */
                    int, /* subject */
                    int /* object */
                    );

}
#endif // CONSTRAINT_H
