#include <iostream>

#include "message.h"

int get_msgqu_id ()
{
  key_t key = ftok ("muncher_ftok_file", 3);
  std::cout << "queue id: "
            << msgget (key, IPC_CREAT | 0770 )
            << std::endl;
  return msgget (key, IPC_CREAT | 0770 );
}
